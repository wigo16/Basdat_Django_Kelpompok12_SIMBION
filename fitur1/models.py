from django.db import models

# Create your models here.
class Pengguna(models.Model):
  class Meta:
    db_table = 'pengguna'
  username=models.CharField(max_length=20, primary_key=True)
  password=models.CharField(max_length=20)
  role=models.CharField(max_length=20)