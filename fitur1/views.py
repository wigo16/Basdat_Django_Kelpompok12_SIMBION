from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Pengguna
from .forms import Pengguna as PenggunaForm

# Create your views here.
response = {}
def login(request):
	if request.method == 'POST':
		form = PenggunaForm(request.POST)
		if form.is_valid():
			username=request.POST["username"]
			p = Pengguna.objects.get(username=username)
			if p.password == request.POST["password"]:
				request.session['login_user'] = username
				return redirect('/')
			else:
				return HttpResponse("Wrong username or password")

	if request.session.get('login_user') is not None:
		return redirect('/')

	response['login'] = True
	html = 'login.html'
	return render(request, html, response)

def logout(request):
	try:
		del request.session['login_user']
	except:
		pass
	return redirect('/')