from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
	response['login'] = True
	html = 'landing_page.html'
	return render(request, html, response)

def lihat_informasi_beasiswa(request):
	response['login'] = True
	html = 'informasi_beasiswa.html'
	return render(request, html, response)	