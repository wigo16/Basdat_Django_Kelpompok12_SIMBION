from django.shortcuts import render
from django.db import connection
# Create your views here.
response = {}
cursor = connection.cursor()

def index(request):
	cursor.execute('SELECT * FROM MAHASISWA')
	mahasiswa = dictfetchall(cursor)
	print(mahasiswa)
	response['mahasiswa'] = mahasiswa
	html = 'Tabel daftar pendaftar beasiswa.html'
	return render(request, html, response)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]