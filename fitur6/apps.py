from django.apps import AppConfig


class ListPendaftarBeasiswaConfig(AppConfig):
    name = 'list_pendaftar_beasiswa'
