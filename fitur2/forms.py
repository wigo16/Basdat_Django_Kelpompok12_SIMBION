from django import forms

class MahasiswaForm(forms.Form):
  npm=forms.CharField(max_length=20)
  email=forms.EmailField(max_length=50)
  nama=forms.CharField(max_length=50)
  no_telp=forms.CharField(max_length=20)
  alamat_tinggal=forms.CharField(max_length=50)
  alamat_domilisi=forms.CharField(max_length=50)
  nama_bank=forms.CharField(max_length=50)
  no_rekening=forms.CharField(max_length=20)
  nama_pemilik=forms.CharField(max_length=50)
  username=forms.CharField(max_length=20)

class DonaturForm(forms.Form):
  nomor_identitas=forms.CharField(max_length=20)
  email=forms.EmailField(max_length=50)
  nama=forms.CharField(max_length=50)
  npwp=forms.CharField(max_length=20)
  no_telp=forms.CharField(max_length=20)
  alamat=forms.CharField(max_length=50)
  username=forms.CharField(max_length=20)

class YayasanForm(forms.Form):
  no_sk_yayasan=forms.CharField(max_length=20)
  yayasan_email=forms.EmailField(max_length=50)
  yayasan_nama=forms.CharField(max_length=50)
  no_telp_cp=forms.CharField(max_length=30)
  nomor_identitas_donatur=forms.CharField(max_length=20)