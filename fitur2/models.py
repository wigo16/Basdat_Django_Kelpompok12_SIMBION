from django.db import models
from fitur1.models import Pengguna

# Create your models here.
class Mahasiswa(models.Model):
  class Meta:
    db_table = 'mahasiswa'
  npm=models.CharField(max_length=20, primary_key=True)
  email=models.EmailField(max_length=50)
  nama=models.CharField(max_length=50)
  no_telp=models.CharField(max_length=20)
  alamat_tinggal=models.CharField(max_length=50)
  alamat_domilisi=models.CharField(max_length=50)
  nama_bank=models.CharField(max_length=50)
  no_rekening=models.CharField(max_length=20)
  nama_pemilik=models.CharField(max_length=50)
  username=models.ForeignKey(Pengguna, on_delete='CASCADE', db_column='username')

class Donatur(models.Model):
  class Meta:
    db_table = 'donatur'
  nomor_identitas=models.CharField(max_length=20, primary_key=True)
  email=models.EmailField(max_length=50)
  nama=models.CharField(max_length=50)
  npwp=models.CharField(max_length=20)
  no_telp=models.CharField(max_length=20)
  alamat=models.CharField(max_length=50)
  username=models.ForeignKey(Pengguna, on_delete='CASCADE', db_column='username')

class Yayasan(models.Model):
  class Meta:
    db_table = 'yayasan'
  no_sk_yayasan=models.CharField(max_length=20, primary_key=True)
  email=models.EmailField(max_length=50)
  nama=models.CharField(max_length=50)
  no_telp_cp=models.CharField(max_length=20)
  nomor_identitas_donatur=models.ForeignKey(Donatur, on_delete='CASCADE', db_column='nomor_identitas_donatur')