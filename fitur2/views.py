from django.shortcuts import render, redirect
from django.http import HttpResponse
from fitur1.models import Pengguna
from fitur1.forms import Pengguna as PenggunaForm
from .models import Mahasiswa, Donatur, Yayasan
from .forms import MahasiswaForm, DonaturForm, YayasanForm

# Create your views here.
response = {}
def index(request):
	if request.method == 'POST':
		if request.POST.get('npm') != None:
			form = MahasiswaForm(request.POST)
			if form.is_valid():
				p = Pengguna.objects.create(username=request.POST['username'], password=request.POST['password'], role='Mahasiswa')
				m = Mahasiswa.objects.create(
					npm=request.POST.get('username'),
					email=request.POST.get('email'),
					nama=request.POST.get('nama'),
					no_telp=request.POST.get('no_telp'),
					alamat_tinggal=request.POST.get('alamat_tinggal'),
					alamat_domilisi=request.POST.get('alamat_domilisi'),
					nama_bank=request.POST.get('nama_bank'),
					no_rekening=request.POST.get('no_rekening'),
					nama_pemilik=request.POST.get('nama_pemilik'),
					username=Pengguna.objects.get(username=p.username)
				)
				request.session['login_user'] = p.username
				return redirect('/')
			else:
				return HttpResponse("Failed create mahasiswa")
		else:
			formDonatur = DonaturForm(request.POST)
			if formDonatur.is_valid():
				p = Pengguna.objects.create(username=request.POST['username'], password=request.POST['password'], role='Donatur')
				d = Donatur.objects.create(
					nomor_identitas=request.POST.get('nomor_identitas'),
					email=request.POST.get('email'),
					nama=request.POST.get('nama'),
					npwp=request.POST.get('npwp'),
					no_telp=request.POST.get('no_telp'),
					alamat=request.POST.get('alamat'),
					username=Pengguna.objects.get(username=p.username),
				)

				if request.POST.get('no_sk_yayasan') != None:
					y = Yayasan.objects.create(
						no_sk_yayasan=request.POST.get('no_sk_yayasan'),
						email=d.email,
						nama=request.POST.get('yayasan_nama'),
						no_telp_cp=d.no_telp,
						nomor_identitas_donatur=Donatur.objects.get(nomor_identitas=d.nomor_identitas),
					)
			else:
				return HttpResponse("Failed create donatur / yayasan")
			request.session['login_user'] = request.POST['username']
			return redirect('/')
	response['login'] = True
	html = 'register.html'
	return render(request, html, response)