"""SIMBION_Basdat_12 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
import fitur1.urls as fitur1
import fitur2.urls as fitur2
import fitur3.urls as fitur3
import fitur5.urls as fitur5
import fitur6.urls as fitur6
import fitur4.urls as fitur4

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include((fitur1, 'fitur1'), namespace='fitur1')),
    url(r'^register/', include((fitur2, 'fitur2'), namespace='fitur2')),
    url(r'^beasiswa-baru/', include((fitur3, 'fitur3'), namespace='fitur3')),
    url(r'^', include((fitur4, 'fitur4'), namespace='fitur4')),
    url(r'^mendaftar-beasiswa/', include((fitur5, 'fitur5'), namespace='fitur5')),
    url(r'^list-beasiswa/', include((fitur6, 'fitur6'), namespace='fitur6'))
]
