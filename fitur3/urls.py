from django.conf.urls import url
from .views import *

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^Membuat-Paket-Beasiswa-Baru', Membuat_Paket_Beasiswa_Baru, name='Membuat_Paket_Beasiswa_Baru'),
    url(r'^Menambahkan_beasiswa_dari_paket', Menambahkan_beasiswa_dari_paket, name='Menambahkan_beasiswa_dari_paket'),
    url(r'^submit_paket_beasiswa_baru', submit_paket_beasiswa_baru, name='submit_paket_beasiswa_baru'),
    url(r'^tambah_beasiswa', tambah_beasiswa, name='tambah_beasiswa'),
]
