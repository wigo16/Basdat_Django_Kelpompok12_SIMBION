from django.apps import AppConfig


class FormDaftarBeasiswaConfig(AppConfig):
    name = 'form_daftar_beasiswa'
